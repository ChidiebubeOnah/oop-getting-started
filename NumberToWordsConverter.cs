﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shege_Management
{
    internal static class NumberToWordsConverter
    {
        private static string[] numberInWords =
            {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve"};

        private static string[] multiplesOfTen = {"", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};

        private static string stringLiterals;

        public static string Convert(int number)
        {
            if (number < 20)
            {

                stringLiterals = numberInWords[number];

                return numberInWords[number];

            }

            if (number < 100)
            {
                if (number % 10 != 0)
                {
                    string remainder = " " + numberInWords[number % 10];

                    stringLiterals = multiplesOfTen[number / 10] + remainder;

                    return stringLiterals;

                }
              
                stringLiterals = multiplesOfTen[number / 10];
                
            }

            return stringLiterals;
        }
    }
}

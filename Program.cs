﻿namespace Shege_Management
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Hello Enter Rachel's number (0 and 99): \n");
            string userInput = string.Empty;

            try
            { userInput = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(userInput))
                {
                    throw new Exception("User Input can't be blank !");
                }

                int number = int.Parse(userInput);

                string result = NumberToWordsConverter.Convert(number);

                Console.WriteLine(result);
            }
            catch (FormatException fE)
            {
                Console.WriteLine("Ops! '{0}' is  not a digit !", userInput);
            }
            catch (Exception e)
            {
                Console.WriteLine("Ops! an error see below for more details: \n{0}\nfor debug see:\n{1}", e.Message, e.StackTrace);

            }

          
        }
    }
}